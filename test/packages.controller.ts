import 'mocha';
import request from 'supertest';
import Server from '../server';
import * as assert from "assert";
import * as faker from "faker";

describe('Packages test cases', () => {

    let packageToBeCreated = createPackage();
    let packageToBeUpdated = createPackage();
    let token = "token";

    let lastId: string = "";

    function assertResourceIsSame(actualResource, expectedResource) {
        assert.strictEqual(actualResource.name, expectedResource.name);
        assert.strictEqual(actualResource.type, expectedResource.type);
        assert.equal(actualResource.location.continent, expectedResource.location.continent);
        assert.equal(actualResource.location.country, expectedResource.location.country);
        assert.equal(actualResource.location.region, expectedResource.location.region);
        assert.equal(actualResource.location.city, expectedResource.location.city);
        assert.equal(actualResource.startDate, expectedResource.startDate);
        assert.equal(actualResource.endDate, expectedResource.endDate);
        assert.equal(actualResource.inventory, expectedResource.inventory);
        assert.strictEqual(actualResource.status, expectedResource.status);
        assert.equal(actualResource.price, expectedResource.price);
        assert.strictEqual(actualResource.description, expectedResource.description);
    }

    it("should be able to create a package", async () => {
        let response = await request(Server)
            .post('/api/v1/packages')
            .set('Authorization', 'Bearer ' + token)
            .send(packageToBeCreated)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        assert.equal(201, response.statusCode);
        assertResourceIsSame(response.body, packageToBeCreated);
        lastId = response.body.id;
    });

    it("would create many packages", async () => {
        let package1 = await request(Server)
            .post('/api/v1/packages')
            .set('Authorization', 'Bearer ' + token)
            .send(createPackage());
        assert.equal(package1.statusCode, 201);
        let package2 = await request(Server)
            .post('/api/v1/packages')
            .set('Authorization', 'Bearer ' + token)
            .send(createPackage());
        assert.equal(package2.statusCode, 201);
        let package3 = await request(Server)
            .post('/api/v1/packages')
            .set('Authorization', 'Bearer ' + token)
            .send(createPackage());
        assert.equal(package3.statusCode, 201);
        let package4 = await request(Server)
            .post('/api/v1/packages')
            .set('Authorization', 'Bearer ' + token)
            .send(createPackage());
        assert.equal(package4.statusCode, 201);
        let package5 = await request(Server)
            .post('/api/v1/packages')
            .set('Authorization', 'Bearer ' + token)
            .send(createPackage());
        assert.equal(package5.statusCode, 201);
    });
    it("should be able to fetch all packages", async () => {
            let response = await request(Server)
                .get('/api/v1/packages')
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            assert.equal(response.statusCode, 200);
            assert.equal(response.body.length, 6);
            assertResourceIsSame(response.body[0], packageToBeCreated);
        }
    );

    it("should be able to fetch a package", async () => {
            let response = await request(Server)
                .get('/api/v1/packages/' + lastId)
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            assert.equal(response.statusCode, 200);
            assertResourceIsSame(response.body, packageToBeCreated);
        }
    );

    it("should be able to update a package", async () => {
            let response = await request(Server)
                .put('/api/v1/packages/' + lastId)
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json')
                .send(packageToBeUpdated);
            assert.equal(response.statusCode, 200);
            assertResourceIsSame(response.body, packageToBeUpdated);
        }
    );

    it("check if the package was updated", async () => {
            let response = await request(Server)
                .get('/api/v1/packages/' + lastId)
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            assert.equal(response.statusCode, 200);
            assertResourceIsSame(response.body, packageToBeUpdated);
        }
    );

    it("should be able to delete a package", async () => {
            let response = await request(Server)
                .get('/api/v1/packages')
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            assert.equal(response.statusCode, 200);
            assert.equal(response.body.length, 6);
            let packageToDelete = response.body[3];
            let deletedPackageResponse = await request(Server)
                .delete('/api/v1/packages/' + packageToDelete.id)
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            assert.equal(deletedPackageResponse.statusCode, 200);
        }
    );

    it("should fetch less packages after the package was deleted", async () => {
        let response = await request(Server)
            .get('/api/v1/packages')
            .set('Authorization', 'Bearer ' + token)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        assert.equal(response.statusCode, 200);
        assert.equal(response.body.length, 5);
    });

    function createPackage() {
        return {
            name: faker.address.country(),
            type: "martial arts",
            status: "active",
            inventory: 20,
            location: {
                continent: faker.address.country(),
                country: faker.address.country(),
                region: faker.address.county(),
                city: faker.address.city()
            },
            startDate: faker.date.recent(3).toLocaleDateString(),
            endDate: faker.date.recent(33).toLocaleDateString(),
            price: faker.finance.amount(800, 2500),
            description: faker.random.words(5)
        };
    }
});
