CREATE TABLE `packages` (
	`aId` INT(11) NOT NULL AUTO_INCREMENT,
	`id` varchar(100) NOT NULL,
	`name` varchar(100) NOT NULL,
	`type` varchar(100) NOT NULL,
	`status` varchar(100) NOT NULL,
	`inventory` smallint NOT NULL,
	`location` json NOT NULL,
	`startDate` DATE NOT NULL,
	`endDate` DATE NOT NULL,
	`price` float NOT NULL,
	`description` varchar(250) NULL DEFAULT NULL,
	PRIMARY KEY (`aId`),
	UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=UTF8MB4;