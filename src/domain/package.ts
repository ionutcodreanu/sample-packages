import {Location} from "./location";

export class Package {
    private _id: string;
    private _name: string;
    private _type: string;
    private _status: string;
    private _inventory: number;
    private _location: Location;
    private _startDate: string;
    private _endDate: string;
    private _price: number;
    private _description: string;

    constructor(name: string,
                type: string,
                status: string,
                inventory: number,
                location: Location,
                startDate: string,
                endDate: string,
                price: number,
                description: string) {
        this._name = name;
        this._type = type;
        this._status = status;
        this._inventory = inventory;
        this._location = location;
        this._startDate = startDate;
        this._endDate = endDate;
        this._price = price;
        this._description = description;
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }

    get status(): string {
        return this._status;
    }

    set status(value: string) {
        this._status = value;
    }

    get inventory(): number {
        return this._inventory;
    }

    set inventory(value: number) {
        this._inventory = value;
    }

    get location(): Location {
        return this._location;
    }

    set location(value: Location) {
        this._location = value;
    }

    get startDate(): string {
        return this._startDate;
    }

    set startDate(value: string) {
        this._startDate = value;
    }

    get endDate(): string {
        return this._endDate;
    }

    set endDate(value: string) {
        this._endDate = value;
    }

    get price(): number {
        return this._price;
    }

    set price(value: number) {
        this._price = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }
}