export type Location = {
    continent: string,
    country: string,
    region: string,
    city: string
}