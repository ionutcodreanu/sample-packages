import {Package} from "./package";

export interface PackageRepository {
    create(packageModel: Package): Promise<Package>;

    all(): Promise<Package[]>;

    byId(id: string): Promise<Package>;

    update(packageModel: Package): Promise<Package>;

    delete(packageModel: Package): Promise<void>;
}