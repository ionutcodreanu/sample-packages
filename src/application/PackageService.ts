import {PackageRepository} from "../domain/packageRepository";
import {Package} from "../domain/package";
import {CreatePackageCommand} from "./CreatePackageCommand";
import {UpdatePackageCommand} from "./UpdatePackageCommand";
import {TYPES} from "../port/types";
import {inject, injectable} from "inversify";

@injectable()
export class PackageService {
    private readonly packageRepository: PackageRepository;

    public constructor(@inject(TYPES.PackagesRepository) packageRepository: PackageRepository) {
        this.packageRepository = packageRepository;
    }

    all(): Promise<Package[]> {
        return this.packageRepository.all();
    }

    create(createPackageCommand: CreatePackageCommand): Promise<Package> {
        const packageModel = new Package(
            createPackageCommand.name,
            createPackageCommand.type,
            createPackageCommand.status,
            createPackageCommand.inventory,
            createPackageCommand.location,
            createPackageCommand.startDate,
            createPackageCommand.endDate,
            createPackageCommand.price,
            createPackageCommand.description,
        );
        return this.packageRepository.create(packageModel);
    }

    byId(packageId: string): Promise<Package> {
        return this.packageRepository.byId(packageId);
    }

    update(command: UpdatePackageCommand): Promise<Package> {
        return this.packageRepository.byId(command.id).then(
            packageModel => {
                packageModel.name = command.name;
                packageModel.type = command.type;
                packageModel.status = command.status;
                packageModel.inventory = command.inventory;
                packageModel.location = command.location;
                packageModel.startDate = command.startDate;
                packageModel.endDate = command.endDate;
                packageModel.price = command.price;
                packageModel.description = command.description;
                return this.packageRepository.update(packageModel);
            }
        );
    }

    delete(packageId: string): Promise<void> {
        return this.packageRepository.byId(packageId).then(
            packageModel => this.packageRepository.delete(packageModel)
        );
    }
}