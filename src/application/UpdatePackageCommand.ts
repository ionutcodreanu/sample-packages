import {CreatePackageCommand} from "./CreatePackageCommand";

export type UpdatePackageCommand = CreatePackageCommand & { id: string };