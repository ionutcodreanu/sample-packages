import {Location} from "../domain/location";

export type CreatePackageCommand = {
    name: string,
    type: string,
    status: string,
    inventory: number,
    location: Location,
    startDate: string, //add value object
    endDate: string, //add value object
    price: number,
    description: string
}