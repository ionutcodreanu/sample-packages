import {Request, Response} from 'express';
import {PackageService} from "../../../application/PackageService";
import {CreatePackageCommand} from "../../../application/CreatePackageCommand";
import {Package} from "../../../domain/package";
import {UpdatePackageCommand} from "../../../application/UpdatePackageCommand";
import {inject, injectable} from "inversify";
import {TYPES} from "../../types";
import * as faker from "faker";
import {Location} from "../../../domain/location";

@injectable()
export class PackagesController {
    private readonly packageService: PackageService;

    constructor(@inject(TYPES.PackagesService) packageService: PackageService) {
        this.packageService = packageService;
    }

    all(req: Request, res: Response): void {
        this.packageService.all().then(response =>
            res.status(200)
                .json(response.map(packageModel => this.mapPackage(packageModel)))
        ).catch(reason => res.status(500));
    }

    create(req: Request, res: Response): void {
        const command: CreatePackageCommand = {
            name: req.body.name,
            type: req.body.type,
            status: req.body.status,
            inventory: req.body.inventory,
            location: req.body.location,
            startDate: req.body.startDate,
            endDate: req.body.endDate,
            price: req.body.price,
            description: req.body.description
        };
        this.packageService.create(command).then(
            response => res.status(201)
                .json(this.mapPackage(response))
        );
    }

    byId(req: Request, res: Response): void {
        const packageId: string = req.params['id'];
        this.packageService.byId(packageId)
            .then(response => res.status(200).json(this.mapPackage(response)))
            .catch(reason => res.status(404).json({}));
    }

    update(req: Request, res: Response): void {
        const packageId: string = req.params['id'];
        const command: UpdatePackageCommand = {
            id: packageId,
            name: req.body.name,
            type: req.body.type,
            status: req.body.status,
            inventory: req.body.inventory,
            location: req.body.location,
            startDate: req.body.startDate,
            endDate: req.body.endDate,
            price: req.body.price,
            description: req.body.description
        };
        this.packageService.update(command).then(
            response => res.status(200)
                .json(this.mapPackage(response))
        ).catch(reason => res.status(404).json({}));
    }

    delete(req: Request, res: Response): void {
        const packageId: string = req.params['id'];
        this.packageService.delete(packageId).then(
            () => res.status(200).json({})
        ).catch(reason => res.status(404).json());
    }

    mapPackage(packageModel: Package): object {
        return {
            id: packageModel.id,
            name: packageModel.name,
            type: packageModel.type,
            status: packageModel.status,
            inventory: packageModel.inventory,
            location: {
                continent: packageModel.location.continent,
                country: packageModel.location.country,
                region: packageModel.location.region,
                city: packageModel.location.city
            },
            startDate: packageModel.startDate,
            endDate: packageModel.endDate,
            price: packageModel.price,
            description: packageModel.description
        };
    }
}
