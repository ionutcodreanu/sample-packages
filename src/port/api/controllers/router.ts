import express from 'express';
import {appContainer} from "../inversify.config";
import {TYPES} from "../../types";
import {PackagesController} from "./PackagesController";

const controller = appContainer.get<PackagesController>(TYPES.PackagesController);
export default express.Router()
    .post('/', (req, res) => controller.create(req, res))
    .get('/', (req, res) => controller.all(req, res))
    .get('/:id', (req, res) => controller.byId(req, res))
    .put('/:id', (req, res) => controller.update(req, res))
    .delete ('/:id', (req, res) => controller.delete(req, res));