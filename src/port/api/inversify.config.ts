import { Container } from "inversify";
import { TYPES } from "../types";
import {PackageService} from "../../application/PackageService";
import {PackageRepository} from "../../domain/packageRepository";
import {PackagesController} from "./controllers/PackagesController";
import Knex from "knex";
import {MySqlRepository} from "../persistence/MySqlRepository";

const appContainer = new Container();

appContainer.bind<Knex>(TYPES.MySqlConnection).toConstantValue(Knex({
    client: "mysql2",
    connection: {
        host: process.env.MYSQL_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DATABASE
    },
}));

appContainer.bind<PackageRepository>(TYPES.PackagesRepository).to(MySqlRepository);
appContainer.bind<PackageService>(TYPES.PackagesService).to(PackageService);
appContainer.bind<PackagesController>(TYPES.PackagesController).to(PackagesController);

export { appContainer };