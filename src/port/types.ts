const TYPES = {
    PackagesService: Symbol.for("PackagesService"),
    PackagesRepository: Symbol.for("PackagesRepository"),
    PackagesController: Symbol.for("PackagesController"),
    MySqlRepository: Symbol.for("MySqlRepository"),
    MySqlConnection: Symbol.for("MySqlConnection")
};

export {TYPES};