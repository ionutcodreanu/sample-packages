import {v4 as uuid} from "uuid";
import {PackageRepository} from "../../domain/packageRepository";
import {Package} from "../../domain/package";
import {injectable} from "inversify";

@injectable()
export class InMemoryPackageRepository implements PackageRepository {
    private localStorage: Package[] = [];

    all(): Promise<Package[]> {
        return Promise.resolve(this.localStorage);
    }

    byId(id: string): Promise<Package> {
        const packageModel = this.localStorage.find(row => row.id == id);
        if (packageModel) {
            return Promise.resolve(packageModel);
        } else {
            return Promise.reject("Package not found");
        }
    }

    create(packageModel: Package): Promise<Package> {
        packageModel.id = uuid();
        this.localStorage.push(packageModel);
        return Promise.resolve(packageModel);
    }

    update(packageModel: Package): Promise<Package> {
        this.localStorage.map(localResource => this.localStorage.find(current => current.id === packageModel.id) || packageModel);
        return Promise.resolve(packageModel);
    }

    delete(packageModel: Package): Promise<void> {
        this.localStorage = this.localStorage.filter(currentResource => currentResource.id != packageModel.id);
        return Promise.resolve();
    }
}