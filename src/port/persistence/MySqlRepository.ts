import * as Knex from "knex";
import {inject, injectable} from "inversify";
import {TYPES} from "../types";
import {v4 as uuid} from "uuid";
import {PackageRepository} from "../../domain/packageRepository";
import {Package} from "../../domain/package";

@injectable()
export class MySqlRepository implements PackageRepository {
    private static readonly tableName = "packages";
    private dbConnection: Knex;

    constructor(@inject(TYPES.MySqlConnection) dbConnection: Knex) {
        this.dbConnection = dbConnection;
    }

    all(): Promise<Package[]> {
        return this.dbConnection
            .select("*")
            .from<Package>(MySqlRepository.tableName)
            .orderByRaw("aId ASC")
            .then(result => Promise.resolve(result.map(row => this.mapRow(row))))
            .catch(reason => Promise.reject(reason));
    }

    byId(id: string): Promise<Package> {
        return this.dbConnection<Package>(MySqlRepository.tableName)
            .where('id', id)
            .first()
            .then(packageModel => this.mapRow(packageModel))
            .catch(reason => Promise.reject(reason));
    }

    create(packageModel: Package): Promise<Package> {
        packageModel.id = uuid();
        return this.dbConnection
            .into(MySqlRepository.tableName)
            .insert({
                    id: packageModel.id,
                    name: packageModel.name,
                    type: packageModel.type,
                    status: packageModel.status,
                    inventory: packageModel.inventory,
                    location: JSON.stringify(packageModel.location),
                    startDate: packageModel.startDate,
                    endDate: packageModel.endDate,
                    price: packageModel.price,
                    description: packageModel.description,
                }
            )
            .then(result => {
                return Promise.resolve(packageModel)
            })
            .catch(reason => {
                return Promise.reject(reason)
            });
    }

    delete(packageModel: Package): Promise<void> {
        return this.dbConnection(MySqlRepository.tableName)
            .where('id', packageModel.id)
            .del();
    }

    update(packageModel: Package): Promise<Package> {
        return this.dbConnection(MySqlRepository.tableName)
            .where('id', packageModel.id)
            .update({
                name: packageModel.name,
                type: packageModel.type,
                status: packageModel.status,
                inventory: packageModel.inventory,
                location: JSON.stringify(packageModel.location),
                startDate: packageModel.startDate,
                endDate: packageModel.endDate,
                price: packageModel.price,
                description: packageModel.description,
            })
            .then(result => {
                return Promise.resolve(packageModel);
            })
            .catch(reason => {
                return Promise.reject(reason);
            });
    }

    private mapRow(row: any): Package {
        let packageModel = new Package(
            row.name,
            row.type,
            row.status,
            row.inventory,
            row.location,
            new Date(row.startDate).toLocaleDateString(),
            new Date(row.endDate).toLocaleDateString(),
            row.price,
            row.description,
        );
        packageModel.id = row.id;
        return packageModel;
    }
}