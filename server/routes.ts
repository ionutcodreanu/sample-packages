import { Application } from 'express';
import packagesRouter from '../src/port/api/controllers/router'
export default function routes(app: Application): void {
  app.use('/api/v1/packages', packagesRouter);
};